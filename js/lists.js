var Days = [31,28,31,30,31,30,31,31,30,31,30,31];// index => month [0-11]
$(document).ready(function(){
    var option = '<option value="day">DIA*</option>';
    var selectedDay="day";
    for (var i=1;i <= Days[0];i++){ //add option days
        option += '<option value="'+ i + '">' + i + '</option>';
    }
    $('#day').append(option);
    $('#day').val(selectedDay);

    var option = '<option value="month">MÊS*</option>';
    var selectedMon ="month";
    for (var i=1;i <= 12;i++){
        option += '<option value="'+ i + '">' + i + '</option>';
    }
    $('#month').append(option);
    $('#month').val(selectedMon);
  
    var d = new Date();
    var option = '<option value="year">ANO*</option>';
    selectedYear ="year";
    for (var i=1930;i <= d.getFullYear();i++){// years start i
        option += '<option value="'+ i + '">' + i + '</option>';
    }
    $('#year').append(option);
    $('#year').val(selectedYear);
});
function isLeapYear(year) {
    year = parseInt(year);
    if (year % 4 != 0) {
	      return false;
	  } else if (year % 400 == 0) {
	      return true;
	  } else if (year % 100 == 0) {
	      return false;
	  } else {
	      return true;
	  }
}

function change_year(select)
{
    if( isLeapYear( $(select).val() ) )
	  {
		    Days[1] = 29;
		    
    }
    else {
        Days[1] = 28;
    }
    if( $("#month").val() == 2)
		    {
			       var day = $('#day');
			       var val = $(day).val();
			       $(day).empty();
			       var option = '<option value="day">day</option>';
			       for (var i=1;i <= Days[1];i++){ //add option days
				         option += '<option value="'+ i + '">' + i + '</option>';
             }
			       $(day).append(option);
			       if( val > Days[ month ] )
			       {
				          val = 1;
			       }
			       $(day).val(val);
		     }
  }

function change_month(select) {
    var day = $('#day');
    var val = $(day).val();
    $(day).empty();
    var option = '<option value="day">day</option>';
    var month = parseInt( $(select).val() ) - 1;
    for (var i=1;i <= Days[ month ];i++){ //add option days
        option += '<option value="'+ i + '">' + i + '</option>';
    }
    $(day).append(option);
    if( val > Days[ month ] )
    {
        val = 1;
    }
    $(day).val(val);
}

//DROPDOWN


function SetupDropdown() {
    var areaDropdown = $('#areaDropdown');
    let cargoDropdown = $('#cargoDropdown');
    let interesseDropdown = $('#interesseDropdown');

    areaDropdown.append('<option value="true">ÁREA*</option>');
    cargoDropdown.append('<option value="true">CARGO*</option>');
    interesseDropdown.append('<option value="true">INTERESSES DE NEGÓCIO</option>');

    var jsonLink = "./JSON/lists.json";

    // Populate dropdown with list of provinces
    $.getJSON(jsonLink, function (data) {
        $.each(data.area, function (key, entry) {
            areaDropdown.append($('<option></option>').attr('value', entry).text(entry));
        })
        $.each(data.cargo, function (key, entry) {
            cargoDropdown.append($('<option></option>').attr('value', entry).text(entry));
        })
        $.each(data.interesse, function (key, entry) {
            interesseDropdown.append($('<option></option>').attr('value', entry).text(entry));
        })
    })
};