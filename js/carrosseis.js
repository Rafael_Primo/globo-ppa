document.addEventListener("DOMContentLoaded", function () {
	new Splide("#cons-car", {
        perPage: 3,
		perMove: 1,
        width: 900,
        fixedHeight: 250,
        gap: 10,
        pagination: false,
		breakpoints: {
			768: {
				perPage: 1,
				width: 280,
				fixedHeight: 250,
			},
			992: {
				perPage: 2,
				width: 600,
				fixedHeight: 250,
			},
		},
		type: 'loop'
	}).mount();

	new Splide("#header-banner", {
		perPage: 1,
		pagination: false,
		type: 'loop'
	}).mount();

	new Splide("#pilulas-car", {
		perPage: 1,
		perMove: 1,
		width: 1152,
		pagination: false,
		breakpoints: {
			768: {
				width: 320,
			},
			992: {
				width: 448,
			},
			1400: {
				width: 896,
			}
		},
		type: 'loop'
	}).mount();
});
