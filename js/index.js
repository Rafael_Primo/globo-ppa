function GetFieldsData()
{
    let name = $("#nome").val();
    let surname = $("#sobrenome").val();
    let company =$("#empresa").val();
    let area =$("#areaDropdown").val();
    let charge =$("#cargoDropdown").val();
    let phone = $("#tel").unmask().val();
    let day =$("#day").val();
    let month =$("#month").val();
    let year =$("#year").val();
    let interest = $("#interesseDropdown").val();
    name = name.replace(/^\s+/, '').replace(/\s+$/, '');
    surname = surname.replace(/^\s+/, '').replace(/\s+$/, '');
    company = company.replace(/^\s+/, '').replace(/\s+$/, '');

    if(login === "" || name === "" || surname === "" || company === "" || area == "true" || charge == "true" || phone === "" || day == "day" || month == "month" || year == "year")
    {
        // console.log("só espaços brancos");
        $('#cadastroError').show();
        $('#cadastroError').text("preencha todos os campos");
    }
    else
    {
        // console.log("tudo preenchido");
        let tName = name.replace(/ +(?= )/g,'');
        let tSurname = surname.replace(/ +(?= )/g,'');
        let tCompany = company.replace(/ +(?= )/g,'');
        if(ValidateText(tName)) 
        {
            $('#cadastroError').show();
            $('#cadastroError').text("o campo de nome contém caracteres inválidos");
        }
        if(ValidateText(tSurname)) 
        {
            $('#cadastroError').show();
            $('#cadastroError').text("o campo de sobrenome contém caracteres inválidos");
        }
        else if(ValidateText(tCompany)) 
        {
            $('#cadastroError').show();
            $('#cadastroError').text("o campo de empresa contém caracteres inválidos");
        }
        
        else if (phone.length < 10) {
            $('#cadastroError').show();
            $('#cadastroError').text("o campo de telefone tem que ter mais de 10 caracteres");
            }
        else
        {
            if(ValidateEmail(login))
            {
                if(!ValidateText(tName) && !ValidateText(tSurname) && !ValidateText(tCompany))
                {
                    if($("#termos-cad").is(":checked"))
                    {
                        $('#loaderStart').show();
                        if($("#news-cad").is(":checked")){
                            PostData(login, tName, tSurname, company, area, charge, phone, day, month, year, interest, 1);
                        } else {
                            PostData(login, tName, tSurname, company, area, charge, phone, day, month, year, interest, 0);
                        }
                    }
                    else 
                    {
                        $('#cadastroError').show();
                        $('#cadastroError').text("aceite os termos para prosseguir");
                    }
                }
            }
            else 
            {
                $('#cadastroError').show();
                $('#cadastroError').text("digite um email valido");
            }
        }
    }
}

function ValidateEmail(_email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,99})+$/.test(_email)) {
        ////console.log("retornou certo");
        return (true);
    }
    ////console.log("retornou errado");
    return (false);
}

function ValidateText(_text) {
    if (/[.,\/#!$%@\\|\/\^&\*;:{}=\+_`'"¨~^()¬§¢£¹²³ªº°<>\[(.*?)\]]/.test(_text)) {
        ////console.log("retornou certo");
        return (true);
    }
    ////console.log("retornou errado");
    return (false);
}

$(function () {
    AddListenerEnter("emailArea", "btnLogin");
    CaptureAnalytics("/login", "Página Login");

    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

	CheckDate("login");
});
    
window.onfocus = function () {
    CheckDate("login");
}

var login;

$("#btnLogin").click(function () {
    var loginErro = document.getElementById("emailError");
    var email = document.getElementById("emailArea").value;
    login = email;
    if (email !== "") {
        if(ValidateEmail(email)){
            if($("#termosLogin").is(":checked"))
            {
                $("#emailError").hide();
                $("#loaderStart").show();
                loader_interval = setTimeout(function () {
                    email = document.getElementById("emailArea").value;
        
                    clearInterval(loader_interval);
        
                    setTimeout(function () {
                        CheckUserAuth(email);
                    }, 200);
                }, 200);
            }
            else{
                $("#emailError").text("Aceite os termos para continuar");
                $("#emailError").show();
            }
        }
        else{
            $("#emailError").text("Digite um e-mail válido/Formato de e-mail inválido");
            $("#emailError").show();
        }

    } else {
        $("#emailError").text("Preencha com seu email para começar");
        $("#emailError").show();
    }
});

window.onload = window.onresize = function () {
    var login = document.getElementById("login");
    var cadastro = document.getElementById("cadastro");
    var cadastrook = document.getElementById("cadastrook");
    var height = window.innerHeight;
    login.style.height = height + "px";
    cadastro.style.height = height + "px";
    cadastrook.style.height = height + "px";
}


$(function() {
    $("form").submit(function() { return false; });
});


function ButtonClick(_case){
	switch (_case) {
		case "privacidade":
			window.open("https://cloud.relacionamentoglobo.globo.com/politica-privacidade/", "_blank");
            CaptureAnalytics("/login/termos", "Clicou no botão termos de privacidade login");
		break;
		case "privacidadeCadastro":
			window.open("https://cloud.relacionamentoglobo.globo.com/politica-privacidade/", "_blank");
            TrackUser(login, "Clicou no botão termos de privacidade cadastro");
            CaptureAnalytics("/cadastro/termos", "Clicou no botão termos de privacidade cadastro");
		break;
	}
}
