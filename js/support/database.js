const apiEndpoint = "https://4l1bsap673.execute-api.sa-east-1.amazonaws.com/dev/";

var HttpClientGet = function () {
    this.get = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("GET", aUrl, true);
        anHttpRequest.send(null);
    }
}
var HttpClientPost = function () {
    this.post = function (aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function () {
            // alert("Status: " + anHttpRequest.status);
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open("POST", aUrl, true);
        anHttpRequest.send(null);
    }
}


function PostData(_Email, _Name, _Surname, _Company, _Area, _Charge, _Phone, _Day, _Month, _Year, _Interest, _News) {
    if(_Interest == "true") _Interest = ""
    let url = apiEndpoint + "insertuser" 
    + "?e=" + _Email.toLowerCase() 
    + "&n=" + _Name 
    + "&s=" + _Surname 
    + "&em=" + _Company 
    + "&a=" + _Area 
    + "&c=" + _Charge 
    + "&t=" + "55" + _Phone 
    + "&d=" + _Day 
    + "&m=" + _Month 
    + "&y=" + _Year 
    + "&i=" + _Interest
    + "&ne=" + _News;
    var client = new HttpClientPost();
    client.post(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp)
        if(temp == 0)
        {
            $("#emailError").show();
            $("#emailError").html("usuário já registrado / e-mail já em uso");
            $("#nome").val("");
            $("#sobrenome").val("");
            $("#empresa").val("");
            $("#tel").val("");
            $('#btnCadastro').prop('disabled', true);
            $('#loaderStart').hide();
            $('#cadastroDiv').hide();
            $('#loginDiv').show();
        }
        else
        {
            $("#email").val("");
            $("#nome").val("");
            $("#sobrenome").val("");
            $("#empresa").val("");
            $("#tel").val("");
            $('#btnCadastro').prop('disabled', true);
            TrackUser(_Email, "Realizou o Cadastro");
            CaptureAnalytics("/Cadastro/Concluido", "Realizou o Cadastro");
            $('#loaderStart').hide();
            $("#emailError").show();
            $("#emailError").html("Cadastro realizado com sucesso");
            $('#cadastroDiv').hide();
            $('#loginDiv').show();
        }
    });
}

function CheckUserAuth(_login) {
    let url = apiEndpoint + "userauth" + "?l=" + _login.toLowerCase();

    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);

        console.log(temp);
        
        if (temp == 1) {
            //email certo
            if (canLogin) {
                location.href = 'home.html?l=' + _login.toLowerCase() + "&on=1";
            } else {
                $("#emailError").text("A plataforma será liberada dia 24 de agosto às 9h50");
                $("#emailError").show();
                $("#loaderStart").hide();
                $("#btnLogin").text("Em breve");
            }
        } else if (temp == 0) {
            //email invalido
            $("#loginDiv").hide();
            $("#cadastroDiv").show();
            CaptureAnalytics("/Cadastro/Cadastro", "Abriu o Cadastro");
            TrackUser(_login, "Abriu o Cadastro");
            $("#loaderStart").hide();
        }
        else if(temp == -1) {
            $("#emailError").html("Houve algum erro no login. Por favor, tente novamente");
            $("#emailUser").show();
            $("#loaderStart").hide();
        }   
        else {
            $("#emailError").html("Houve algum erro no login. Por favor, tente novamente");
            $("#emailUser").show();
            $("#loaderStart").hide();
        }
    });
}

function CheckUserOnline(_login, _online) {
    let url = apiEndpoint + "userauth/onauth" + "?l=" + _login.toLowerCase() + "&on=" + _online;

    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);

        if (temp == -1) {
            //console.log("senha incorreta");
            location.href = 'index.html';
        } else if (temp == 0) {
            //console.log("email incorreto");
            location.href = 'index.html';
        } else if (temp == 1) {
            // console.log("dados conferem");
            var newURL = removeParam("l", window.location.href);
            newURL = removeParam("on", newURL);
            newURL = newURL.replace("?", "");
            window.history.pushState('', document.title, newURL);
            GetLinks("live");

            TrackUser(_login, "Entrou na home");
            $("#loaderStart").hide();
        } else {
            location.href = 'index.html';
        }
    });
}



function TrackUser(_login, _action) {
    let url = apiEndpoint + "usertracking" + "?l=" + _login.toLowerCase() + "&a=" + _action;

    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp);

        if (temp == 1) {
            // console.log("inseriu no banco");
        } else {
            // console.log("houve algum problema");
        }
    });
}


function GetLinks(_Video) {
    let url = apiEndpoint + "getlink" + "?v=" + _Video;
    var client = new HttpClientGet();
    client.get(url, function (response) {
        let temp = JSON.parse(response);
        // console.log(temp)
        OpenModal(temp, true);
    });
}


var m_VimeoLive;
function OpenModal(_url) {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {                
        $('#iframeModal').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    } else {
        $('#iframeModal').attr('src', _url+ "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0");
    }
    if (iOS()) {
        $('#iframeModal').attr('src', _url + "?autoplay=0&loop=0&autopause=0&playsinline=1&portrait=0&title=0&fullscreen=0&muted=1");
    }
    $('#iframeModal').on("load", function () {
        $("#loaderIframe").hide();
        m_VimeoLive = new Vimeo.Player($('#iframeModal'));
        setTimeout(() => {
            m_VimeoLive.play();
        }, 200);
    });
}

function PlayVideo(){
    var m_VimeoUniqueLive = new Vimeo.Player($('#iframeModalUnique'));

    m_VimeoUniqueLive.play();
    TrackUser(login, "Clicou no botão aceito na live");
    CaptureAnalytics("/home/live/aceito", "Clicou no botão aceito na live");
}

function CloseConteudo()
{
    $('#modalVideo').modal('hide');
    $('#iframeModal').removeClass("iframeModalMB");
    setTimeout(() => {
        $('#iframeModal').off("load")
        $("#iframeModal").attr("src", " ");
    }, 300);
}

function AddListenerEnter(field, button) {
    input = document.getElementById(field);
    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            document.getElementById(button).click();
        }
    });
}

function iOS() {
    return [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ].includes(navigator.platform) || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}


function ReplaceSpecialCharacters(textarea) {
    var textSlice
    if(textarea.value.length > 560){
        textarea.value = textarea.value.slice(0, 560);
    }
    textarea.value = textarea.value.replace(/[&\/\\#+~'"*<>{}_|@$=§^`´%¨)(ªº¹²³;]/g, '');
}

