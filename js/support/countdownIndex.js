var days;
var hours;
var minutes;
var seconds;

var x;
var now;

var stopCounter = false;
var canLogin = false;

var lastValue = null;

function CheckDate(_val) {
    if (!stopCounter) {
        let url = apiEndpoint + "servertime" + "?v=" + _val;

        var client = new HttpClientGet();

        client.get(url, function (response) {
            let temp = JSON.parse(response);
            console.log(temp)
            Counter(temp);
        });
    }
}

function Counter(_timestamp) {  
  hours = Math.floor((_timestamp / (1000 * 60 * 60)));
  minutes = Math.floor((_timestamp % (1000 * 60 * 60)) / (1000 * 60));
  seconds = Math.floor((_timestamp % (1000 * 60)) / 1000);

  if (hours < 10) {
    hours = "0" + hours;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (seconds < 10) {
    seconds = "0" + seconds;
  }

  now = _timestamp;

  clearInterval(x);

  x = setInterval(function () {

        now = now - 1000;

        var distance = now;

        Cronometro(distance);
    }, 1000);
}

function Cronometro(distance) {
    hours = Math.floor((distance / (1000 * 60 * 60)));
    minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    seconds = Math.floor((distance % (1000 * 60)) / 1000);  
    
    if (hours < 10) {
    hours = "0" + hours;
    }
    if (minutes < 10) {
    minutes = "0" + minutes;
    }
    if (seconds < 10) {
    seconds = "0" + seconds;
    }
    if (distance < 0) {
        clearInterval(x);
        canLogin = true;
        stopCounter = true;
        $("#btnLogin").text("Tô dentro!");
    } 
    else {
    }
}